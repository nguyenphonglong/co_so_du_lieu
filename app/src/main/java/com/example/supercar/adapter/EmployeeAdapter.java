package com.example.supercar.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.supercar.R;
import com.example.supercar.data.dataemployee.Employee;
import com.example.supercar.data.dataemployee.EmployeeController;
import com.example.supercar.ui.detailemployee.DetailEmployeeActivity;
import com.example.supercar.utils.listener.OnItemClickListener;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Employee> employees;
    private EmployeeController employeeController;

    private OnItemClickListener<Employee> onItemClickListener;

    public EmployeeAdapter(Context context, ArrayList<Employee> employees, EmployeeController employeeController) {
        this.context = context;
        this.employees =employees;
        this.employeeController = employeeController;
    }

    public void setOnItemClickListener(OnItemClickListener<Employee> onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }




    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_employee, parent, false);
        return new ViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Employee employee=employees.get(position);
        holder.tvEmployeeName.setText(employee.getName());
        holder.tvEmployeePosition.setText(employee.getPosition());

        byte[] bitmapData=employee.getImage();
        if (bitmapData != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
            holder.ivEmployee.setImageBitmap(bitmap);
        } else {
            holder.ivEmployee.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.employee));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onItemClick(employee);
                } else {
                    Intent intent = new Intent(context, DetailEmployeeActivity.class);

                    intent.putExtra("EMPLOYEE_ID", employee.getId());

                    context.startActivity(intent);

                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return employees.size();
    }


    public void replaceData(ArrayList<Employee> employees) {
        this.employees.clear();
        this.employees.addAll(employees);
        notifyDataSetChanged();
    }



    class ViewHolder extends RecyclerView.ViewHolder {

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        TextView tvEmployeeName = itemView.findViewById(R.id.tv_employee_name);
        TextView tvEmployeePosition = itemView.findViewById(R.id.tv_employee_position);
        CircleImageView ivEmployee =(CircleImageView) itemView.findViewById(R.id.iv_employee);


    }

}
