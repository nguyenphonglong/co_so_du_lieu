package com.example.supercar.ui.detailattendance;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.supercar.R;
import com.example.supercar.base.BaseActivity;
import com.example.supercar.data.dataemployee.Employee;
import com.example.supercar.data.dataemployee.EmployeeController;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DetailAttendanceActivity extends BaseActivity {

    private ImageView ivBack;
    private TextView tvCall, tvSMS;
    private EmployeeController employeeController;
    private int employeeID = -1;
    private Employee employee;
    ImageView ivCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_attendance);

        employeeID = getIntent().getIntExtra("EMPLOYEE_ID", -1);
        employeeController = new EmployeeController(this, null);

        if (employeeID < 0) {
            finish();
        }

        ivCall=findViewById(R.id.iv_call);
        ivBack = findViewById(R.id.iv_back);
        tvCall = findViewById(R.id.tv_call);
        tvSMS = findViewById(R.id.tv_send_sms);
        addEvent();
    }

    private void addEvent() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        final String phone;
        employee = employeeController.selectEmployeeByID(employeeID);
        phone = employee.getPhone().toString();
        tvCall.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {
                String phoneNumber = String.format("tel: "+phone);
                // Create the intent.
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                // Set the data for the intent as the phone number.
                dialIntent.setData(Uri.parse(phoneNumber));
                // If package resolves to an app, send intent.
                if (dialIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(dialIntent);
                }
            }
        });
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        final String formattedDate = df.format(c);

        tvSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.fromParts("sms",phone,null));
                intent.putExtra("sms_body", formattedDate +": "+getString(R.string.attandanceEmployeeEveryday));
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (employeeID > 0) {
            employee = employeeController.selectEmployeeByID(employeeID);

        }
    }
}
