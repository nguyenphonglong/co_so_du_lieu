package com.example.supercar.ui.addproduct;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.supercar.R;
import com.example.supercar.data.dataproduct.Product;
import com.example.supercar.data.dataproduct.ProductController;
import com.example.supercar.data.dataproduct.ProductNavigator;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class AddProductActivity extends AppCompatActivity implements ProductNavigator {

    Button btnTakePhoto, btnSelectPhoto, btnCancel,btnSave;
    ImageView ivProduct,ivBack;
    EditText edtName,edtProducer,edtPrice,edtDescription,edtAddNewType;
    Spinner spinnerAddType;

    final int RESQUEST_TAKE_PHOTO = 123;
    final int REQUEST_CHOOSE_PHOTO = 321;

    private ProductController productController;
    private String type = "";
    private ArrayList<String> allTypes;
    private Bitmap bitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        productController = new ProductController(this, this);
        addControls();
        addEvents();

    }

    private void addEvents() {
        ivProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogUploadImage();
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtName.getText().toString().isEmpty()|| edtProducer.getText().toString().isEmpty()||
                edtPrice.getText().toString().isEmpty()||edtDescription.getText().toString().isEmpty()){
                    Toast.makeText(AddProductActivity.this,R.string.stringInvalidData,Toast.LENGTH_SHORT).show();
                } else {
                    if(type.isEmpty()) type = edtAddNewType.getText().toString();
                    if (bitmap == null){
                        productController.addProduct(new Product(edtName.getText().toString(),edtProducer.getText().toString(),
                                type.trim(),Double.parseDouble(edtPrice.getText().toString()),
                                edtDescription.getText().toString(),null));
                    } else {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        bitmap.recycle();
                        productController.addProduct(new Product(edtName.getText().toString(),edtProducer.getText().toString(),
                                    type.trim(),Double.parseDouble(edtPrice.getText().toString()),
                                    edtDescription.getText().toString(),byteArray));
                    }
                    Toast.makeText(AddProductActivity.this,R.string.stringAddProudctCucessfull,Toast.LENGTH_SHORT).show();
                    finish();
                }


            }
        });

        //Spinner
        allTypes = productController.selectAllType();
        if (allTypes == null) allTypes = new ArrayList();
        allTypes.add(getString(R.string.stringAddNewType));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, allTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerAddType.setAdapter(adapter);
        spinnerAddType.setSelection(0);
        spinnerAddType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == allTypes.size() - 1) {
                    edtAddNewType.setVisibility(View.VISIBLE);
                    type = "";
                    edtAddNewType.requestFocus();
                } else {
                    edtAddNewType.setVisibility(View.GONE);
                    type = allTypes.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });



    }

    private void addControls() {
        btnCancel=findViewById(R.id.btn_add_product_cancel);
        btnSave=findViewById(R.id.btn_add_product_save);
        ivBack=findViewById(R.id.iv_back);
        ivProduct=findViewById(R.id.iv_add_product);
        edtName=findViewById(R.id.edt_add_product_name);
        edtProducer=findViewById(R.id.edt_add_product_producer);
        edtPrice=findViewById(R.id.edt_add_product_price);
        edtDescription=findViewById(R.id.edt_add_product_description);
        spinnerAddType=findViewById(R.id.spin_product_type);
        edtAddNewType=findViewById(R.id.edt_add_new_type);
    }


    private void openDialogUploadImage() {
        final Dialog dialog = new Dialog(AddProductActivity.this);
        dialog.setContentView(R.layout.dialog_upload_image);
        btnTakePhoto = (Button) dialog.findViewById(R.id.btn_take_photo);
        btnSelectPhoto = (Button) dialog.findViewById(R.id.btn_select_photo);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                dialog.dismiss();
            }
        });
        btnSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosePhoto();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RESQUEST_TAKE_PHOTO);
    }

    private void choosePhoto() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CHOOSE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CHOOSE_PHOTO) {
                try {
                    Uri imageUri = data.getData();
                    InputStream is = getContentResolver().openInputStream(imageUri);
                    bitmap = BitmapFactory.decodeStream(is);
                    ivProduct.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == RESQUEST_TAKE_PHOTO) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ivProduct.setImageBitmap(bitmap);
            }
        }
    }




    @Override
    public void displayProducts(ArrayList<Product> products) {

    }
}
