package com.example.supercar.ui.addemployee;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.supercar.R;
import com.example.supercar.data.dataemployee.Employee;
import com.example.supercar.data.dataemployee.EmployeeController;
import com.example.supercar.data.dataemployee.EmployeeNavigator;
import com.example.supercar.ui.listemployee.ListEmployeeEActivity;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddEmployeeActivity extends AppCompatActivity implements EmployeeNavigator {

    Button btnTakePhoto, btnSelectPhoto, btnSave,btnCancel;
    ImageView ivBack;
    CircleImageView ivEmployee;
    EditText edtName,edtPhone,edtEmail,edtPosition;
    Spinner spinnerGender;
    final int RESQUEST_TAKE_PHOTO = 123;
    final int REQUEST_CHOOSE_PHOTO = 321;

    private EmployeeController employeeController;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_employee);

        employeeController=new EmployeeController(this,this);
        addControls();
        addEvents();

    }

    private void addEvents() {
        ivEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogUploadImage();
            }
        });
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtName.getText().toString().isEmpty() || edtPhone.getText().toString().isEmpty() ||
                        edtEmail.getText().toString().isEmpty()||edtPosition.getText().toString().isEmpty()){
                    Toast.makeText(AddEmployeeActivity.this,R.string.stringInvalidData,Toast.LENGTH_SHORT).show();
                }else {
                    if(bitmap==null){
                        employeeController.addEmployee(new Employee(edtName.getText().toString(),
                                String.valueOf(spinnerGender.getSelectedItem()),edtPhone.getText().toString(),
                                edtEmail.getText().toString(),edtPosition.getText().toString(),null));
                    }else {
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();
                        bitmap.recycle();
                        employeeController.addEmployee(new Employee(edtName.getText().toString(),
                                String.valueOf(spinnerGender.getSelectedItem()),edtPhone.getText().toString(),
                                edtEmail.getText().toString(),edtPosition.getText().toString(),byteArray));
                    }
                    Toast.makeText(AddEmployeeActivity.this,R.string.stringAddedEmployee,Toast.LENGTH_SHORT).show();
                    finish();
                }

            }
        });

    }


    private void addControls() {
        btnSave=findViewById(R.id.btn_add_employee_save);
        btnCancel=findViewById(R.id.btn_add_employee_cancel);
        ivBack = findViewById(R.id.iv_back);
        edtName=findViewById(R.id.edt_add_employee_name);
        edtPhone=findViewById(R.id.edt_add_employee_phone);
        edtEmail=findViewById(R.id.edt_add_employee_email);
        edtPosition=findViewById(R.id.edt_add_employee_position);
        ivEmployee=(CircleImageView) findViewById(R.id.iv_add_employee);
        spinnerGender=findViewById(R.id.spin_gender);

    }

    private void openDialogUploadImage() {
        final Dialog dialog = new Dialog(AddEmployeeActivity.this);
        dialog.setContentView(R.layout.dialog_upload_image);
        btnTakePhoto = (Button) dialog.findViewById(R.id.btn_take_photo);
        btnSelectPhoto = (Button) dialog.findViewById(R.id.btn_select_photo);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                dialog.dismiss();
            }
        });
        btnSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosePhoto();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RESQUEST_TAKE_PHOTO);
    }

    private void choosePhoto() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CHOOSE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CHOOSE_PHOTO) {
                try {
                    Uri imageUri = data.getData();
                    InputStream is = getContentResolver().openInputStream(imageUri);
                    bitmap = BitmapFactory.decodeStream(is);
                    ivEmployee.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == RESQUEST_TAKE_PHOTO) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ivEmployee.setImageBitmap(bitmap);
            }
        }
    }


    @Override
    public void displayEmployees(ArrayList<Employee> employees) {

    }
}
