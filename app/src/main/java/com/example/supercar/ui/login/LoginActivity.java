package com.example.supercar.ui.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import com.example.supercar.R;
import com.example.supercar.ui.home.MainActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private final static String DEFAULT_USERNAME = "admin";
    private final static String DEFAULT_PASSWORD = "5";

    private TextView txtSignUp;
    private EditText txtAccount, txtPassword;
    private Button btnLogin;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initViewComponent();
        initUI();

    }

    private void initViewComponent() {
        txtAccount = findViewById(R.id.txtAcount);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(this);

    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (isCorrectAcount()){
                    startActivity(new Intent(this, MainActivity.class));
                    break;
                }else {
                    Toast.makeText(this,getString(R.string.loginStatus),Toast.LENGTH_LONG).show();
                    break;
                }


        }
    }

    private boolean isCorrectAcount(){
        return (txtAccount.getText().toString().equals(DEFAULT_USERNAME)&& txtPassword.getText().toString().equals(DEFAULT_PASSWORD));

    }

    private void initUI() {
        //
    }


}
