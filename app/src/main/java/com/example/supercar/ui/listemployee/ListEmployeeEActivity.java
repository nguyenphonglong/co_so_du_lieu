package com.example.supercar.ui.listemployee;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.supercar.R;
import com.example.supercar.adapter.EmployeeAdapter;
import com.example.supercar.base.BaseFragment;
import com.example.supercar.data.dataemployee.Employee;
import com.example.supercar.data.dataemployee.EmployeeController;
import com.example.supercar.data.dataemployee.EmployeeNavigator;
import com.example.supercar.ui.addemployee.AddEmployeeActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class ListEmployeeEActivity extends AppCompatActivity implements EmployeeNavigator {

    //View components
    private RecyclerView rvEmployees;
    private FloatingActionButton fabAdd;
    private ImageView ivBack;

    ////Variables
    private EmployeeController employeeController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_employee_e);

        addControls();
        addEvents();
    }


    private void addEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListEmployeeEActivity.this, AddEmployeeActivity.class);
                startActivity(intent);

            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            employeeController.getAllEmployees();
        }

    }


    private void addControls() {
        employeeController = new EmployeeController(this, this);
        rvEmployees = findViewById(R.id.rv_employees);
        fabAdd = findViewById(R.id.fab_add_employee);
        ivBack=findViewById(R.id.iv_back);

        EmployeeAdapter employeeAdapter = new EmployeeAdapter(this, new ArrayList<Employee>(), employeeController);
        rvEmployees.setAdapter(employeeAdapter);
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onResume() {
        super.onResume();

        if (employeeController != null) {
            employeeController.getAllEmployees();
        }
    }


    @Override
    public void displayEmployees(ArrayList<Employee> employees) {
        Log.d("__EMPLOYEE", employees.size() + "");
        EmployeeAdapter employeeAdapter = (EmployeeAdapter) rvEmployees.getAdapter();
        if (employeeAdapter != null) {
            employeeAdapter.replaceData(employees);
        }

    }
}
