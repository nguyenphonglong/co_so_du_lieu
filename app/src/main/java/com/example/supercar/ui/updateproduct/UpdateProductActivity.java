package com.example.supercar.ui.updateproduct;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.supercar.R;
import com.example.supercar.data.dataproduct.Product;
import com.example.supercar.data.dataproduct.ProductController;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class UpdateProductActivity extends AppCompatActivity  {

    Button btnTakePhoto, btnSelectPhoto, btnCancel,btnSave;
    ImageView ivProduct,ivBack;
    EditText edtName,edtProducer,edtPrice,edtDescription,edtAddNewType;
    Spinner spinnerType;

    final int RESQUEST_TAKE_PHOTO = 123;
    final int REQUEST_CHOOSE_PHOTO = 321;

    private ProductController productController;
    private String type = "";
    private ArrayList<String> allTypes;
    private Bitmap bitmap;

    private int productID = -1;
    private Product product;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_product);

        productController = new ProductController(this, null);

        productID = getIntent().getIntExtra("PRODUCT_ID", -1);
        if (productID < 0) {
            finish();
        }

        product = productController.selectProductByID(productID);

        initViewComponent();
        initUI();
        addEvent();
    }

    private void addEvent() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        ivProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogUploadImage();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProduct();
            }
        });
    }


    private void initViewComponent() {
        ivBack = findViewById(R.id.iv_back);
        ivProduct=findViewById(R.id.iv_update_product);
        edtName=findViewById(R.id.edt_update_product_name);
        edtProducer=findViewById(R.id.edt_update_product_producer);
        edtAddNewType=findViewById(R.id.edt_update_new_type);
        edtPrice=findViewById(R.id.edt_update_product_price);
        edtDescription=findViewById(R.id.edt_update_product_description);
        btnCancel=findViewById(R.id.btn_update_product_cancel);
        btnSave=findViewById(R.id.btn_update_product_save);

        spinnerType=findViewById(R.id.spin_update_product_type);

    }

    private void initUI() {

        initSpinner();

        byte[] bitmapData = product.getImage();
        if (bitmapData != null) {
            bitmap = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
            ivProduct.setImageBitmap(bitmap);
        } else {
            ivProduct.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.product));
        }

        edtName.setText(product.getName());
        edtProducer.setText(product.getProducer());
        edtPrice.setText(String.valueOf(product.getPrice()));
        edtDescription.setText(product.getDescription());

    }


    private void initSpinner() {
        //Spinner
        allTypes = productController.selectAllType();
        if (allTypes == null) allTypes = new ArrayList();
        allTypes.add(getString(R.string.stringAddNewType));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, allTypes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adapter);
        spinnerType.setSelection(0);
        spinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if (position == allTypes.size() - 1) {
                    edtAddNewType.setVisibility(View.VISIBLE);
                    type = "";
                    edtAddNewType.requestFocus();
                } else {
                    edtAddNewType.setVisibility(View.GONE);
                    type = allTypes.get(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });
    }

    private void updateProduct() {
        if(edtName.getText().toString().isEmpty()||edtProducer.getText().toString().isEmpty()||
            edtPrice.getText().toString().isEmpty()||edtDescription.getText().toString().isEmpty()){
            Toast.makeText(UpdateProductActivity.this,getString(R.string.stringInvalidData),Toast.LENGTH_SHORT);
        }else {
            if(type.isEmpty()) type=edtAddNewType.getText().toString();
            product.setName(edtName.getText().toString());
            product.setProducer(edtProducer.getText().toString());
            product.setType(type.trim());
            product.setPrice(Double.parseDouble(edtPrice.getText().toString()));
            product.setDescription(product.getDescription());
            if (bitmap == null) {
                product.setImage(null);
            } else {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                bitmap.recycle();
                product.setImage(byteArray);
            }

            productController.update(product);
            finish();

        }
    }

    private void openDialogUploadImage() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_upload_image);
        Button btnTakePhoto = (Button) dialog.findViewById(R.id.btn_take_photo);
        Button btnSelectPhoto = (Button) dialog.findViewById(R.id.btn_select_photo);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                dialog.dismiss();
            }
        });
        btnSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosePhoto();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RESQUEST_TAKE_PHOTO);
    }

    private void choosePhoto() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CHOOSE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CHOOSE_PHOTO) {
                try {
                    Uri imageUri = data.getData();
                    InputStream is = getContentResolver().openInputStream(imageUri);
                    bitmap = BitmapFactory.decodeStream(is);
                    ivProduct.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == RESQUEST_TAKE_PHOTO) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ivProduct.setImageBitmap(bitmap);
            }
        }
    }



}
