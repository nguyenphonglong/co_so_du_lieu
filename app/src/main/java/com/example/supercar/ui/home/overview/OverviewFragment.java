package com.example.supercar.ui.home.overview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.example.supercar.R;
import com.example.supercar.base.BaseFragment;

public class OverviewFragment extends BaseFragment implements View.OnClickListener {



    @Override
    public void onClick(View view) {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_overview_fragment;
    }
}
