package com.example.supercar.ui.detailproduct;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.supercar.R;
import com.example.supercar.base.BaseActivity;
import com.example.supercar.data.dataproduct.Product;
import com.example.supercar.data.dataproduct.ProductController;
import com.example.supercar.ui.updateproduct.UpdateProductActivity;

public class DetailProductActivity extends BaseActivity implements View.OnClickListener {

    private ImageView ivBack,ivProduct;
    private TextView tvName,tvProducer,tvType,tvPrice,tvDescription;
    private Button btnUpdate,btnDelete;

    private int productID = -1;
    private Product product;

    private ProductController productController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);

        productID = getIntent().getIntExtra("PRODUCT_ID", -1);
        productController = new ProductController(this, null);

        if (productID < 0) {
            finish();
        }

        initViewComponent();

    }

    private void initViewComponent() {
        ivBack = findViewById(R.id.iv_back);

        tvName=findViewById(R.id.tv_detail_product_name);
        tvProducer=findViewById(R.id.tv_detail_product_producer);
        tvType=findViewById(R.id.tv_detail_product_type);
        tvPrice=findViewById(R.id.tv_detail_prodcut_price);
        tvDescription=findViewById(R.id.tv_detail_product_desription);
        btnDelete=findViewById(R.id.btn_delete_product);
        btnUpdate=findViewById(R.id.btn_update_product);

        ivProduct=findViewById(R.id.iv_detail_product);

        ivBack.setOnClickListener(this);
        btnDelete.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (productID > 0) {
            product = productController.selectProductByID(productID);
            if (product != null) {
                initUI();
            }
        }
    }


    private void initUI() {

        byte[] bitmapData = product.getImage();
        if (bitmapData != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
            ivProduct.setImageBitmap(bitmap);
        } else {
            ivProduct.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.product));
        }

        tvName.setText(product.getName());
        tvProducer.setText(product.getProducer());
        tvType.setText(product.getType());
        tvPrice.setText(getString(R.string.product_price, product.getPrice()));
        tvDescription.setText(product.getDescription());

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;

            case R.id.btn_delete_product:
                new AlertDialog.Builder(this)
                        .setTitle(R.string.stringConfirmDelete)
                        .setMessage(getString(R.string.stringDoYouWant)+product.getName()+getString(R.string.fromListProduct))
                        .setCancelable(true)
                        .setPositiveButton(getString(R.string.stringYes), new DialogInterface.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.N)
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                productController.deleteProduct(product);
                                Toast.makeText(DetailProductActivity.this, getString(R.string.stringDeleted),Toast.LENGTH_SHORT).show();
                                DetailProductActivity.this.finish();
                            }
                        })
                        .setNegativeButton(getString(R.string.stringNo), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();
                break;

            case R.id.btn_update_product:
                Intent intent = new Intent(this, UpdateProductActivity.class);

                intent.putExtra("PRODUCT_ID", productID);

                startActivity(intent);
                break;
        }
    }





}
