package com.example.supercar.ui.detailemployee;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.supercar.R;
import com.example.supercar.base.BaseActivity;
import com.example.supercar.data.dataemployee.Employee;
import com.example.supercar.data.dataemployee.EmployeeController;
import com.example.supercar.ui.updateemployee.UpdateEmployeeActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class DetailEmployeeActivity extends BaseActivity {

    private CircleImageView ivDetailEmployee;
    ImageView ivBack;
    private TextView tvDetailEmployeeName,tvDetailEmployeeGender,tvDetailEmployeePhone,
                     tvDetailEmployeeEmail,tvDetailEmployeePosition;
    Button btnUpdate,btnDelete;

    private int employeeID=-1;
    private Employee employee;

    private EmployeeController employeeController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_employee);

        employeeID=getIntent().getIntExtra("EMPLOYEE_ID",-1);
        employeeController=new EmployeeController(this,null);

        if(employeeID<0){
            finish();
        }

        initViewComponent();
        addEvent();
    }

    private void addEvent() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(DetailEmployeeActivity.this)
                        .setTitle(R.string.stringConfirmDelete)
                        .setMessage(getString(R.string.stringDoYouWant)+" "+employee.getName()+" "+getString(R.string.stringFromListEmployee))
                        .setCancelable(true)
                        .setPositiveButton(getString(R.string.stringYes), new DialogInterface.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.N)
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                employeeController.deleteEmployee(employee);
                                Toast.makeText(DetailEmployeeActivity.this, getString(R.string.stringDeleted),Toast.LENGTH_SHORT).show();
                                DetailEmployeeActivity.this.finish();
                            }
                        })
                        .setNegativeButton(getString(R.string.stringNo), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .show();

            }
        });
        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailEmployeeActivity.this, UpdateEmployeeActivity.class);
                intent.putExtra("EMPLOYEE_ID", employeeID);
                startActivity(intent);
            }
        });

    }

    private void initViewComponent() {
        ivBack = findViewById(R.id.iv_back);
        ivDetailEmployee=(CircleImageView) findViewById(R.id.iv_detail_employee);
        tvDetailEmployeeName=findViewById(R.id.tv_detail_employee_name);
        tvDetailEmployeeGender=findViewById(R.id.tv_detail_employee_gender);
        tvDetailEmployeePhone=findViewById(R.id.tv_detail_employee_phone);
        tvDetailEmployeeEmail=findViewById(R.id.tv_detail_employee_email);
        tvDetailEmployeePosition=findViewById(R.id.tv_detail_employee_position);
        btnUpdate=findViewById(R.id.btn_update_employee);
        btnDelete=findViewById(R.id.btn_delete_employee);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (employeeID > 0) {
            employee = employeeController.selectEmployeeByID(employeeID);
            if (employee != null) {
                initUI();
            }
        }
    }

    private void initUI() {
        byte[] bitmapData = employee.getImage();
        if (bitmapData != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
            ivDetailEmployee.setImageBitmap(bitmap);
        } else {
            ivDetailEmployee.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.employee));
        }

        tvDetailEmployeeName.setText(employee.getName());
        tvDetailEmployeeGender.setText(employee.getGender());
        tvDetailEmployeePhone.setText(employee.getPhone());
        tvDetailEmployeeEmail.setText(employee.getEmail());
        tvDetailEmployeePosition.setText(employee.getPosition());
    }


}
