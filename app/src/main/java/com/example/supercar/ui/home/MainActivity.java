package com.example.supercar.ui.home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.supercar.R;
import com.example.supercar.adapter.SimplePagerAdapter;
import com.example.supercar.base.BaseActivity;
import com.example.supercar.ui.home.employee.EmployeeFragment;
import com.example.supercar.ui.home.order.OrderFragment;
import com.example.supercar.ui.home.other.OtherFragment;
import com.example.supercar.ui.home.overview.OverviewFragment;
import com.example.supercar.ui.home.product.ProductFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener, ViewPager.OnPageChangeListener {

    //Fragments
    private OverviewFragment overviewFragment;
    private OrderFragment orderFragment;
    private ProductFragment productFragment;
    private EmployeeFragment employeeFragment;
    private OtherFragment otherFragment;

    //View components
    private ViewPager vpMain;
    private TextView toolbarTitle;
    private BottomNavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViewComponent();
        initUI();

    }

    private void initViewComponent() {
        vpMain = findViewById(R.id.vp_main);
        toolbarTitle = findViewById(R.id.tv_toolbar_title);
        navigationView = findViewById(R.id.navigation);
    }

    private void initUI() {
        setupNavigation();
        setupViewPager();
    }

    private void setupNavigation() {
        navigationView.setOnNavigationItemSelectedListener(this);
    }

    private void setupViewPager(){
        SimplePagerAdapter pagerAdapter = new SimplePagerAdapter(getSupportFragmentManager());
        overviewFragment=new OverviewFragment();
        orderFragment=new OrderFragment();
        productFragment=new ProductFragment();
        employeeFragment=new EmployeeFragment();
        otherFragment=new OtherFragment();

        pagerAdapter.addFragment(overviewFragment,getString(R.string.stringTitleOverview));
        pagerAdapter.addFragment(orderFragment,getString(R.string.stringTitleOrder));
        pagerAdapter.addFragment(productFragment,getString(R.string.stringTitleProduct));
        pagerAdapter.addFragment(employeeFragment,getString(R.string.stringTitleEmployee));
        pagerAdapter.addFragment(otherFragment,getString(R.string.stringTitleOther));

        vpMain.setAdapter(pagerAdapter);
        vpMain.setOffscreenPageLimit(pagerAdapter.getCount());
        vpMain.setCurrentItem(0);

        toolbarTitle.setText(getString(R.string.stringOverview));

        vpMain.addOnPageChangeListener(this);

    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        switch (position) {
            case 0:
                navigationView.setSelectedItemId(R.id.menuOverview);
                break;
            case 1:
                navigationView.setSelectedItemId(R.id.menuOrder);
                break;
            case 2:
                navigationView.setSelectedItemId(R.id.menuProduct);
                break;
            case 3:
                navigationView.setSelectedItemId(R.id.menuEmployee);
                break;
            case 4:
                navigationView.setSelectedItemId(R.id.menuOther);
                break;
        }
        SimplePagerAdapter adapter = (SimplePagerAdapter) vpMain.getAdapter();
        if (adapter != null)
            toolbarTitle.setText(adapter.getPageTitle(position));

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menuOverview:
                vpMain.setCurrentItem(0);
                return true;
            case R.id.menuOrder:
                vpMain.setCurrentItem(1);
                return true;
            case R.id.menuProduct:
                vpMain.setCurrentItem(2);
                return true;
            case R.id.menuEmployee:
                vpMain.setCurrentItem(3);
                return true;
            case R.id.menuOther:
                vpMain.setCurrentItem(4);
                return true;
        }
        return false;

    }
}
