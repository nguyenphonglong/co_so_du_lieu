package com.example.supercar.ui.updateemployee;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.supercar.R;
import com.example.supercar.data.dataemployee.Employee;
import com.example.supercar.data.dataemployee.EmployeeController;
import com.example.supercar.ui.addemployee.AddEmployeeActivity;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;

public class UpdateEmployeeActivity extends AppCompatActivity {

    private ImageView ivBack;
    private CircleImageView ivEmployee;
    private EditText edtName,edtPhone,edtPosition,edtEmail;
    Spinner spinnerGender;
    final int RESQUEST_TAKE_PHOTO = 123;
    final int REQUEST_CHOOSE_PHOTO = 321;
    Button btnTakePhoto, btnSelectPhoto, btnSave,btnCancel;

    private EmployeeController employeeController;
    private Bitmap bitmap;

    private int employeeID = -1;
    private Employee employee;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_employee);

        employeeController=new EmployeeController(this,null);
        employeeID = getIntent().getIntExtra("EMPLOYEE_ID", -1);
        if (employeeID < 0) {
            finish();
        }

        employee = employeeController.selectEmployeeByID(employeeID);

        addControls();
        initUI();
        addEvents();
    }

    private void initUI() {
        byte[] bitmapData = employee.getImage();
        if (bitmapData != null) {
            bitmap = BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
            ivEmployee.setImageBitmap(bitmap);
        } else {
            ivEmployee.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.employee));
        }
        edtName.setText(employee.getName());
        edtPhone.setText(employee.getPhone());
        edtEmail.setText(employee.getEmail());
        edtPosition.setText(employee.getPosition());
        String[] items = getResources().getStringArray(R.array.arrayGender);
        if (employee.getGender().toString().equals(items[0])){
            spinnerGender.setSelection(0);
        }else {
            spinnerGender.setSelection(1);
        }

    }

    private void addEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateEmployee();
            }
        });
        ivEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogUploadImage();
            }
        });
    }

    private void updateEmployee(){
        if(edtName.getText().toString().isEmpty() || edtPhone.getText().toString().isEmpty() ||
                edtEmail.getText().toString().isEmpty()||edtPosition.getText().toString().isEmpty()){
            Toast.makeText(UpdateEmployeeActivity.this,R.string.stringInvalidData,Toast.LENGTH_SHORT).show();
        }else{
            employee.setName(edtName.getText().toString());
            employee.setGender(String.valueOf(spinnerGender.getSelectedItem()));
            employee.setPhone(edtPhone.getText().toString());
            employee.setEmail(edtEmail.getText().toString());
            employee.setPosition(edtPosition.getText().toString());
            if (bitmap == null) {
                employee.setImage(null);
            } else {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                bitmap.recycle();
                employee.setImage(byteArray);
            }

        }
        employeeController.updateEmployee(employee);
        finish();
    }

    private void addControls() {
        btnSave=findViewById(R.id.btn_update_employee_save);
        btnCancel=findViewById(R.id.btn_update_employee_cancel);
        ivBack = findViewById(R.id.iv_back);
        edtName=findViewById(R.id.edt_update_employee_name);
        edtPhone=findViewById(R.id.edt_update_employee_phone);
        edtEmail=findViewById(R.id.edt_update_employee_email);
        edtPosition=findViewById(R.id.edt_update_employee_position);
        ivEmployee=(CircleImageView) findViewById(R.id.iv_update_employee);
        spinnerGender=findViewById(R.id.spin_update_gender);

    }


    private void openDialogUploadImage() {
        final Dialog dialog = new Dialog(UpdateEmployeeActivity.this);
        dialog.setContentView(R.layout.dialog_upload_image);
        btnTakePhoto = (Button) dialog.findViewById(R.id.btn_take_photo);
        btnSelectPhoto = (Button) dialog.findViewById(R.id.btn_select_photo);
        btnTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
                dialog.dismiss();
            }
        });
        btnSelectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosePhoto();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, RESQUEST_TAKE_PHOTO);
    }

    private void choosePhoto() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_CHOOSE_PHOTO);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CHOOSE_PHOTO) {
                try {
                    Uri imageUri = data.getData();
                    InputStream is = getContentResolver().openInputStream(imageUri);
                    bitmap = BitmapFactory.decodeStream(is);
                    ivEmployee.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == RESQUEST_TAKE_PHOTO) {
                bitmap = (Bitmap) data.getExtras().get("data");
                ivEmployee.setImageBitmap(bitmap);
            }
        }
    }
}
