package com.example.supercar.ui.home.employee;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.supercar.R;
import com.example.supercar.base.BaseFragment;
import com.example.supercar.ui.attendance.AttendanceActivity;
import com.example.supercar.ui.listemployee.ListEmployeeEActivity;

public class EmployeeFragment extends BaseFragment  {

    private TextView tvDisplayEmployee, tvAttendanceEmployee;

    @Override
    protected void initUI() {
        tvDisplayEmployee = getView().findViewById(R.id.tv_display_employee);
        tvAttendanceEmployee = getView().findViewById(R.id.tv_attendance_employee);

        tvDisplayEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), ListEmployeeEActivity.class));
            }
        });
        tvAttendanceEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), AttendanceActivity.class));
            }
        });
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_employee_fragment;
    }
}
