package com.example.supercar.ui.detailorder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


import com.example.supercar.R;
import com.example.supercar.adapter.DetailOrderAdapter;
import com.example.supercar.base.BaseActivity;
import com.example.supercar.data.dataorder.Order;
import com.example.supercar.data.dataorder.OrderController;
import com.example.supercar.data.dataorder.OrderDetail;
import com.example.supercar.data.dataproduct.Product;
import com.example.supercar.data.dataproduct.ProductController;

import java.util.ArrayList;

public class DetailOrderActivity extends BaseActivity implements View.OnClickListener {

    private ImageView ivBack, ivPaid;
    private TextView tvCustomer, tvAddress, tvPhone, tvSumPrice,tvPackageOrder;
    private RecyclerView rvOrderProducts;
    private Button btnChangeOrderStatus, btnDeleteOrder;

    private OrderController orderController;

    private int orderID;
    private Order order;
    private ArrayList<OrderDetail> orderDetails;
    private boolean isPaid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);

        orderController = new OrderController(this);

        orderID = getIntent().getIntExtra("ORDER_ID", -1);
        if (orderID < 0) {
            finish();
        }
        order = orderController.getOrderByID(orderID);
        isPaid = order.isPaid();
        orderDetails = order.getListProduct();

        orderController = new OrderController(this);

        initViewComponent();
        initUI();

    }

    private void initUI() {
        tvCustomer.setText(order.getCustomer());
        tvPhone.setText(order.getPhone());
        tvAddress.setText(order.getAddress());
        tvPackageOrder.setText(getString(R.string.stringPackage)+": "+order.getPackage_order());
        bindPaid();

        initRecyclerView();
        initSumPrice();

    }

    private void initRecyclerView() {
        DetailOrderAdapter detailOrderAdapter = new DetailOrderAdapter(this, orderDetails, true);

        rvOrderProducts.setAdapter(detailOrderAdapter);
    }

    private void bindPaid() {
        ivPaid.setVisibility(isPaid ? View.VISIBLE : View.GONE);
        btnChangeOrderStatus.setText(getString(isPaid ? R.string.notCompletePayment : R.string.completedPayment));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
            case R.id.btn_change_order_status:
                new AlertDialog.Builder(this)
                        .setMessage(getString(isPaid ? R.string.notCompletePayment : R.string.completedPayment))
                        .setPositiveButton(getString(R.string.stringYes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                isPaid = !isPaid;
                                order.setPaid(isPaid);
                                orderController.updateOrder(order);
                                bindPaid();
                            }
                        })
                        .setNegativeButton(getString(R.string.stringNo), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
                break;
            case R.id.btn_delete_order:
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.stringConfirmDelete))
                        .setMessage(getString(R.string.confirmDeleteOrder))
                        .setPositiveButton(getString(R.string.stringYes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                orderController.deleteOrder(order, null);
                                finish();
                            }
                        })
                        .setNegativeButton(getString(R.string.stringNo), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
        }
    }




    private void initSumPrice() {
        ProductController productController = new ProductController(this, null);
        double sumPrice = 0;
        for (OrderDetail orderDetail : orderDetails) {
            Product product = productController.selectProductByID(orderDetail.getProductID());
            sumPrice += product.getPrice() * orderDetail.getAmount();
        }
        tvSumPrice.setText(String.valueOf(sumPrice));
    }




    private void initViewComponent() {
        ivBack = findViewById(R.id.iv_back);
        ivPaid = findViewById(R.id.iv_paid);
        tvCustomer = findViewById(R.id.tv_customer);
        tvPhone = findViewById(R.id.tv_phone_number);
        tvAddress = findViewById(R.id.tv_address);
        rvOrderProducts = findViewById(R.id.rv_order_products);
        btnChangeOrderStatus = findViewById(R.id.btn_change_order_status);
        btnDeleteOrder = findViewById(R.id.btn_delete_order);
        tvSumPrice = findViewById(R.id.tv_sum_price);
        tvPackageOrder=findViewById(R.id.tv_package_order);

        ivBack.setOnClickListener(this);
        btnChangeOrderStatus.setOnClickListener(this);
        btnDeleteOrder.setOnClickListener(this);

    }
}
