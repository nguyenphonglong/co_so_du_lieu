package com.example.supercar.ui.attendance;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.supercar.R;
import com.example.supercar.adapter.AttendanceAdapter;
import com.example.supercar.adapter.EmployeeAdapter;
import com.example.supercar.data.dataemployee.Employee;
import com.example.supercar.data.dataemployee.EmployeeController;
import com.example.supercar.data.dataemployee.EmployeeNavigator;
import com.example.supercar.ui.addemployee.AddEmployeeActivity;
import com.example.supercar.ui.listemployee.ListEmployeeEActivity;

import java.util.ArrayList;

public class AttendanceActivity extends AppCompatActivity implements EmployeeNavigator {

    private RecyclerView rvEmployees;
    private ImageView ivBack;

    private EmployeeController employeeController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance);

        addControls();
        addEvents();
    }

    private void addControls() {
        employeeController = new EmployeeController(this, this);
        rvEmployees = findViewById(R.id.rv_attandance_employees);
        ivBack=findViewById(R.id.iv_back);

        AttendanceAdapter attendanceAdapter = new AttendanceAdapter(this, new ArrayList<Employee>(), employeeController);
        rvEmployees.setAdapter(attendanceAdapter);
    }

    private void addEvents() {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            employeeController.getAllEmployees();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onResume() {
        super.onResume();

        if (employeeController != null) {
            employeeController.getAllEmployees();
        }
    }

    @Override
    public void displayEmployees(ArrayList<Employee> employees) {
        Log.d("__EMPLOYEE", employees.size() + "");
        AttendanceAdapter attendanceAdapter = (AttendanceAdapter) rvEmployees.getAdapter();
        if (attendanceAdapter != null) {
            attendanceAdapter.replaceData(employees);
        }

    }
}
