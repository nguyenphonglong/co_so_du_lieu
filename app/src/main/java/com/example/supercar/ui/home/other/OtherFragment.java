package com.example.supercar.ui.home.other;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.supercar.R;
import com.example.supercar.base.BaseFragment;
import com.example.supercar.ui.addproduct.AddProductActivity;
import com.example.supercar.ui.home.MainActivity;
import com.example.supercar.ui.login.LoginActivity;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import java.util.Locale;
import android.content.*;
public class OtherFragment extends BaseFragment {

    Spinner spinLanguage;
    private TextView tvLogout;
    Button btnApply;


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_other_fragment;
    }

    @Override
    protected void initUI() {
        tvLogout = getView().findViewById(R.id.tv_logout);
        spinLanguage=getView().findViewById(R.id.spin_language);
        btnApply=getView().findViewById(R.id.btn_apply_language);

        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), LoginActivity.class));
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String lang= String.valueOf(spinLanguage.getSelectedItem());
                String[] items = getResources().getStringArray(R.array.language);
                if(lang.equals(items[0])){
                    spinLanguage.setSelection(0);
                    ganNgonNgu("en");
                }else {
                    spinLanguage.setSelection(1);
                    ganNgonNgu("vi");
                }
            }
        });

    }

    public void ganNgonNgu(String language) {
        Locale locale = new Locale(language);
        Configuration config = new Configuration();
        config.locale = locale;
        getContext().getResources().updateConfiguration(config,
                getContext().getResources().getDisplayMetrics());

        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
    }

}
