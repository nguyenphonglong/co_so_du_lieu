package com.example.supercar.utils.listener;

public interface OnItemClickListener<T> {
    void onItemClick(T item);
}

