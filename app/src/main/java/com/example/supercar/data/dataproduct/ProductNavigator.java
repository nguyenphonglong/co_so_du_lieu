package com.example.supercar.data.dataproduct;

import java.util.ArrayList;

public interface ProductNavigator {
    void displayProducts(ArrayList<Product> products);
}

