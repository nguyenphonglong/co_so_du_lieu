package com.example.supercar.data.dataemployee;

public class Employee {
    private int id;
    private String name;
    private String gender;
    private String phone;
    private String email;
    private String position;
    private byte[] image;

    public Employee(int id, String name, String gender, String phone, String email, String position, byte[] image) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.position = position;
        this.image = image;
    }

    public Employee(String name, String gender, String phone, String email, String position, byte[] image) {
        this.name = name;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.position = position;
        this.image = image;
    }

    public Employee(String name, String position) {
        this.name = name;
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
