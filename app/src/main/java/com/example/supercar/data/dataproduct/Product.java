package com.example.supercar.data.dataproduct;

public class Product {

    private int id;
    private String name;
    private String producer;
    private String type;
    private double price;
    private String description;
    private byte[] image;

    public Product(int id, String name, String producer, String type, double price, String description, byte[] image) {
        this.id = id;
        this.name = name;
        this.producer = producer;
        this.type = type;
        this.price = price;
        this.description = description;
        this.image = image;
    }

    public Product(String name, String producer, String type, double price, String description, byte[] image) {
        this.id = -1;
        this.name = name;
        this.producer = producer;
        this.type = type;
        this.price = price;
        this.description = description;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProducer() {
        return producer;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public Product(String name, double price) {
        this.id = -1;
        this.name = name;
        this.price = price;
    }


}
