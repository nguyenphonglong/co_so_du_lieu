package com.example.supercar.data.dataorder;

public class OrderDetail {
    private int productID;
    private int amount;

    public OrderDetail(int productID, int amount) {
        this.productID = productID;
        this.amount = amount;
    }

    public int getProductID() {
        return productID;
    }

    public void setProductID(int productID) {
        this.productID = productID;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}
