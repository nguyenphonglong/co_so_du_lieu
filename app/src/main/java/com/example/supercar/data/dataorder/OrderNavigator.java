package com.example.supercar.data.dataorder;

import java.util.ArrayList;

public interface OrderNavigator {
    void showListOrder(ArrayList<Order> orders);
}
