package com.example.supercar.data.dataemployee;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.supercar.data.StoreDatabase;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.concurrent.Callable;
import io.reactivex.functions.Consumer;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class EmployeeController {
    private final static String TABLE_NAME = "employee";

    private final static String COL_ID = "id";
    private final static String COL_NAME = "name";
    private final static String COL_GENDER = "gender";
    private final static String COL_PHONE = "phone";
    private final static String COL_EMAIL = "email";
    private final static String COL_POSITION = "position";
    private final static String COL_IMAGE = "image";

    private Context context;
    private EmployeeNavigator employeeNavigator;
    private StoreDatabase storeDatabase;
    private CompositeDisposable compositeDisposable;

    public EmployeeController(Context context, EmployeeNavigator employeeNavigator) {
        this.context = context;
        this.employeeNavigator = employeeNavigator;
        this.storeDatabase = new StoreDatabase(context);

        compositeDisposable = new CompositeDisposable();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void getAllEmployees() {
        compositeDisposable.clear();
        compositeDisposable.add(
                selectAllEmployeesObservable()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new Consumer<ArrayList<Employee>>() {
                            @Override
                            public void accept(ArrayList<Employee> employees) {
                                if (employeeNavigator != null) employeeNavigator.displayEmployees(employees);
                            }
                        })
        );
    }


    private Observable<ArrayList<Employee>> selectAllEmployeesObservable() {
        return Observable.fromCallable(new Callable<ArrayList<Employee>>() {
            @Override
            public ArrayList<Employee> call() throws Exception {
                return selectAllEmployees();
            }
        });
    }

    private ArrayList<Employee> selectAllEmployees() {
        String query = "SELECT * FROM " + TABLE_NAME;
        return selectEmployeesByQuery(query);
    }


    private ArrayList<Employee> selectEmployeesByQuery(String query) {

        Log.d("__EMPLOYEE", "Query = " + query);
        ArrayList<Employee> employees = new ArrayList<>();

        try {
            SQLiteDatabase database = storeDatabase.getWritableDatabase();

            Cursor cursor = database.rawQuery(query, null);

            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                employees.add(employeeByCursor(cursor));
                cursor.moveToNext();
            }

            cursor.close();
        } catch (SQLiteException exception) {
            exception.printStackTrace();
        }

        return employees;
    }

    private Employee employeeByCursor(Cursor cursor) {
        return new Employee(
                cursor.getInt(cursor.getColumnIndex(COL_ID)),
                cursor.getString(cursor.getColumnIndex(COL_NAME)),
                cursor.getString(cursor.getColumnIndex(COL_GENDER)),
                cursor.getString(cursor.getColumnIndex(COL_PHONE)),
                cursor.getString(cursor.getColumnIndex(COL_EMAIL)),
                cursor.getString(cursor.getColumnIndex(COL_POSITION)),
                cursor.getBlob(cursor.getColumnIndex(COL_IMAGE))

        );
    }

    public void addEmployee(Employee employee) {
        SQLiteDatabase database = storeDatabase.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_NAME, employee.getName());
        contentValues.put(COL_GENDER, employee.getGender());
        contentValues.put(COL_PHONE, employee.getPhone());
        contentValues.put(COL_EMAIL, employee.getEmail());
        contentValues.put(COL_POSITION, employee.getPosition());
        contentValues.put(COL_IMAGE, employee.getImage());

        database.insert(TABLE_NAME, null, contentValues);
    }

    public Employee selectEmployeeByID(int id) {
        SQLiteDatabase database = storeDatabase.getWritableDatabase();

        Cursor cursor = database.query(
                TABLE_NAME,
                new String[]{COL_ID, COL_NAME, COL_GENDER, COL_PHONE, COL_EMAIL, COL_POSITION, COL_IMAGE},
                COL_ID + "=?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null,
                null);
        if (cursor != null)
            cursor.moveToFirst();

        Employee employee = new Employee(
                Integer.parseInt(cursor.getString(0)),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                cursor.getBlob(6)
        );

        cursor.close();

        return employee;
    }

    public int updateEmployee(Employee employee) {
        SQLiteDatabase database = storeDatabase.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(COL_NAME, employee.getName());
        contentValues.put(COL_GENDER, employee.getGender());
        contentValues.put(COL_PHONE, employee.getPhone());
        contentValues.put(COL_EMAIL, employee.getEmail());
        contentValues.put(COL_POSITION, employee.getPosition());
        contentValues.put(COL_IMAGE, employee.getImage());

        // updating row
        return database.update(TABLE_NAME, contentValues, COL_ID + " = ?",
                new String[]{String.valueOf(employee.getId())});
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void deleteEmployee(Employee employee) {
        SQLiteDatabase database = storeDatabase.getWritableDatabase();

        database.delete(TABLE_NAME, COL_ID + " = ?",
                new String[]{String.valueOf(employee.getId())});

        getAllEmployees();
        database.close();
    }




}
