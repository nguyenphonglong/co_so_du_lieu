package com.example.supercar.data.dataemployee;

import java.util.ArrayList;

public interface EmployeeNavigator {
    void displayEmployees(ArrayList<Employee> employees);
}
